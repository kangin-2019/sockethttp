package com.xxx.socketserver;

/**
 * @author khb
 * @date 2021/3/4
 */
public class ServerStart {
    public static void main(String[] args) {
        new ServerListeningThread(8888,10).start();
    }
}
