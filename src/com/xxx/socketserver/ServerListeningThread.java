package com.xxx.socketserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author khb
 * @date 2021/3/4
 */
public class ServerListeningThread extends Thread{
    //设定绑定端口
    private int bindPort;

    //设置最大连接数
    private int n;

    //设置服务器Socket
    private ServerSocket serverSocket;

    public ServerListeningThread(int port,int n) {
        this.bindPort = port;
        this.n = n;
    }

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(bindPort,n);
            //服务器一直监听着
            while (true) {
                Socket rcvSocket = serverSocket.accept();

                //单独写一个类，处理接收的Socket，类的定义在下面
                HttpRequestHandler request = new HttpRequestHandler(rcvSocket);
                request.handle();

                rcvSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //最后要确保以下把ServerSocket关闭掉
            if (serverSocket != null && !serverSocket.isClosed()) {
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
