package com.xxx.socketserver;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * @author khb
 * @date 2021/3/4
 */
public class HttpRequestHandler {
    private Socket socket;

    public HttpRequestHandler(Socket socket) {
        this.socket = socket;
    }

    public void handle() throws IOException {
        StringBuilder builder = new StringBuilder();
        InputStreamReader isr = new InputStreamReader(socket.getInputStream());
        char[] charBuf = new char[1024];
        int mark = -1;
        while ((mark = isr.read(charBuf)) != -1) {
            builder.append(charBuf, 0, mark);
            if (mark < charBuf.length) {
                break;
            }
        }
        if (mark == -1) {
            return;
        }
        String[] splits = builder.toString().split("\r\n");
        String[] splitsT = splits[0].split(" ");
        String[] splitsTs = splitsT[1].split("\\?");
        long result = 0;
        if(splitsTs.length == 2) {
            String[] twoNum = splitsTs[1].split("&");
            String[] nums1 = twoNum[0].split("=");
            long num1 = Integer.parseInt(nums1[1]);
            String[] nums2 = twoNum[1].split("=");
            long num2 = Integer.parseInt(nums2[1]);
          if("/add".equals(splitsTs[0])) {
                result = num1 + num2;
          }
          if("/mult".equals(splitsTs[0])){
              result = num1 * num2;
          }
        }

        socket.getOutputStream().
                write(("HTTP/1.1 200 OK\r\n" +
                        "Content-Type: text/html; charset=utf-8\r\n" +
                        "\r\n" +
                        "<h1>"+result+"</h1>\r\n").getBytes());
    }




}
